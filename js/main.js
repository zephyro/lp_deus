// Navs

$(function() {
    var pull = $('.navbar-toggle');
    var menu = $(pull.attr("data-target"));

    $(pull).on('click', function(e) {
        e.preventDefault();
        menu.slideToggle(50);
    });
});



$('.people-mobile').slick({
    dots: false,
    arrows: false,
    loop: true,
    mobileFirst: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    responsive: [

        {
            breakpoint: 1200,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        }
    ]
});

$('.img-slider').slick({
    dots: false,
    arrows: true,
    loop: true,
    mobileFirst: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '<span class="img-slider-nav prev"></span>',
    nextArrow: '<span class="img-slider-nav next"></span>'
});


$('.img-slider a').click(function(e) {
    e.preventDefault();
});


$(".btn-modal").fancybox({
    'width'      : '100%',
    'padding'    : 0,
    'tpl'        : {
        closeBtn : '<a title="Close" class="btn-close" href="javascript:;"></a>',
        wrap     : '<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner fancybox-scroll"></div></div></div></div>'
    }
});

$('.service-link-sm').click(function(e) {
    e.preventDefault();
    $(this).closest('li').find('.service-content').show();
});

$('.btn-service-form').click(function(e) {
    e.preventDefault();
    $(this).closest('li').find('.service-form').show();
});

$('.info-close').click(function(e) {
    e.preventDefault();
    $(this).closest('.service-info').hide();
});


$('.btn-order').click(function(e) {
    e.preventDefault();
    $.fancybox.close();
});

// Табы 2-й слайд

$('.btn-info-toggle').click(function(e) {
    e.preventDefault();
    var box = $(this).closest('.people-item');
    box.toggleClass('active');
});


// Чекбоксы

$("input[type='radio']").ionCheckRadio();
$("input[type='checkbox']").ionCheckRadio();

$(function () {
    $('.select-style').ikSelect({
        autoWidth: false,
        ddFullWidth: false,
        dynamicWidth: false,
        equalWidths: true,
        extractLink: false,
        linkCustomClass: '',
        ddCustomClass: '',
        filter: false,
        ddMaxHeight: 300,
        customClass: 'select-main'
    });
});
